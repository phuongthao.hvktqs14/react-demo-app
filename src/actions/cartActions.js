import { cartApi } from '../api/cartApi';
import {
    CART_ADD_ITEM,
} from '../constants/cartConstants';

// import products from '../components/products';

export const addCart = (id, qty) => async (dispatch, getState) => {
  const data = await cartApi.add(id);
  dispatch({
    type: CART_ADD_ITEM,
    payload: {
      product: data._id,
      name: data.name,
      image: data.image,
      price: data.price,
      countInStock: data.countInStock,
      qty
    }
  })
  localStorage.setItem('cartItems', JSON.stringify(getState().cart.cartItems))
}

export const removeFromCart = (id) => async (dispatch) => {

}