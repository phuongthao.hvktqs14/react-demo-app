import * as React from 'react';
import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';

import ProductItem from './ProductItem.js';

const PruductList = (props) => {
  return (
    <Grid container justifyContent="center" spacing={4}>
      {props.products.map((product) => (
        <Grid key={product._id} item>
          <Paper
            sx={{
              minHeight: 160,
              minWidth: 320,
              backgroundColor: (theme) =>
                theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
            }}
          >
            <ProductItem product={product}/>
          </Paper>
        </Grid>
      ))}
    </Grid>
  );
}

export default PruductList;