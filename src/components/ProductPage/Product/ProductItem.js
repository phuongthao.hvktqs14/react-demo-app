import * as React from 'react';
import Paper from '@mui/material/Paper';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import { Link } from 'react-router-dom';

import BasicRating from '../../UI/Rating/rating.js';

const ProductItem = ({product}) => {
  return (
    <Paper
      sx={{
        minHeight: 160,
        minWidth: 320,
        backgroundColor: (theme) =>
          theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
      }}
    > 
      <Link to={'/products/' + product._id}>
        <CardMedia
          component="img"
          alt={product.name}
          height="220"
          image={product.image}
        />
      </Link>
      <CardContent>
        <Link to={'/products/' + product._id}>
          <Typography variant="body2" color="text.secondary">
            <strong>{product.name}</strong>
          </Typography>
        </Link>
        <Typography gutterBottom component="div" variant="body2" color="text.secondary" >
          <BasicRating value={product.rating} numReviews={product.numReviews}></BasicRating>
        </Typography>
        <Typography gutterBottom variant="h6" component="div" pl={25}>
          <strong>${product.price}</strong>
        </Typography>
      </CardContent>
    </Paper>
  );
}

export default ProductItem;