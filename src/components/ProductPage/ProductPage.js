import React, { useState, useEffect } from 'react';
import { styled } from '@mui/material/styles';
import { Link, useParams, useNavigate  } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';
import Fab from '@mui/material/Fab';
import List from '@mui/material/List'
import ListItem from '@mui/material/ListItem';
import Divider from '@mui/material/Divider';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import ReplyAllIcon from '@mui/icons-material/ReplyAll';
import AddShoppingCartIcon from '@mui/icons-material/AddShoppingCart';
import CircularProgress from '@mui/material/CircularProgress';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';

import BasicRating from '../UI/Rating/rating.js';
import NotFound from '../NotFoundPage/NotFound.js';
// import products from '../products.js';
import { detailProduct } from '../../actions/productActions.js';

const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: 'center',
  color: theme.palette.text.secondary,
}));

const ProductDetail = ({}) => {
  const [qty, setQty] = useState(1);
  const { id } = useParams();
  const dispatch = useDispatch(); 
  const navigate = useNavigate();
  const productDetail = useSelector(state => state.productDetail)
  const { error, loading, product } = productDetail;

  useEffect(() => {
    dispatch(detailProduct(id))
  }, [dispatch, id])
  
  const addToCartHandler = () => {
    navigate(`/cart/${id}?qty=${qty}`)
  }

  return (
    <Box sx={{ flexGrow: 1 }}>
      {
        loading ? <Grid container justifyContent="center" pt={30}><CircularProgress disableShrink /></Grid>
          : error ? <h3> {error}</h3> :
          (<Grid container spacing={2} p={2}>
            <Grid item xs={8}>
              <Item>
                <Link to={'/products/' + product._id}>
                  <CardMedia
                    component="img"
                    alt={product.name}
                    height="auto"
                    image={product.image}
                  />
                </Link>
              </Item>
            </Grid>
            <Grid item xs={4}>
              <Item>
                <nav aria-label="main mailbox folders">
                  <List  sx={{ height: '20ch' }}>
                    <ListItem disablePadding>
                      <Typography variant="h4" component="div" gutterBottom color="text.secondary">
                        <strong>{product.name}</strong>
                      </Typography>
                    </ListItem>
                  </List>
                </nav>
                <Divider />
                <nav aria-label="main mailbox folders">
                  <List>
                    <ListItem disablePadding>
                      <Typography gutterBottom component="div" variant="body2" color="text.secondary" mt={-2}>
                        <BasicRating value={product.rating} numReviews={product.numReviews}></BasicRating>
                      </Typography>
                    </ListItem>
                  </List>
                </nav>   
                <Divider />     
                <nav aria-label="main mailbox folders">
                  <List>
                    <ListItem disablePadding>  
                      <Typography variant="h5" component="div" pt={2} pb={2}>
                      Price: <strong>${product.price}</strong>
                      </Typography>
                      <FormControl sx={{ ml: 8, minWidth: 120 }} size="small">
                        <InputLabel id="demo-select-small">Quantity</InputLabel>
                        <Select
                          labelId="demo-select-small"
                          id="demo-select-small"
                          value={qty}
                          label="Quantity"
                          onChange={(e) => setQty(e.target.value)}
                        >
                          <MenuItem value="">
                            <em>None</em>
                          </MenuItem>
                          {
                            [...Array(product.countInStock).keys()].map((x) => (
                              <MenuItem key={x+1} value={x+1}>{x+1}</MenuItem>
                            ))
                          }
                        </Select>
                      </FormControl>
                    </ListItem>
                  </List>
                </nav>
                <Divider />
                <nav aria-label="main mailbox folders">
                  <List sx={{ height: '20ch' }}>
                    <ListItem disablePadding>
                      <Typography variant="h7" component="div" pb={2}>
                        Status: {product.countInStock > 0 ? 'In Stock' : 'Out of Stock'}
                      </Typography>
                    </ListItem>
                    <ListItem disablePadding>
                      <Typography variant="h7" component="div">
                        Description: {!!!product && product.description.length > 250 ?
                        `${product.description.substring(0, 250)}...` : product.description 
                        }
                      </Typography>
                    </ListItem>
                  </List>
                </nav>
              </Item>
              <Divider />
              <Box sx={{ '& > :not(style)': { m: 1 } }} pl={4} pt={4}>
                <Fab variant="extended">
                  <ReplyAllIcon sx={{ mr: 1 }} />
                  <Link to='/'>Go home</Link>
                </Fab>
                <Fab variant="extended" color="primary" onClick={() => addToCartHandler()} disabled={product.countInStock == 0}>
                  <AddShoppingCartIcon sx={{ mr: 1 }} />
                  Add cart
                </Fab>            
              </Box>
            </Grid>
          </Grid>)
      }
    </Box>
  );
}

export default ProductDetail;