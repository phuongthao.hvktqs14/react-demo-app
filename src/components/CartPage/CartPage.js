import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useParams, useLocation } from 'react-router-dom';
import Box from '@mui/material/Box';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import ImageListItem from '@mui/material/ImageListItem';
import Avatar from '@mui/material/Avatar';
import ImageIcon from '@mui/icons-material/Image';
import IconButton from '@mui/material/IconButton';
import DeleteIcon from '@mui/icons-material/Delete';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Grid from '@mui/material/Grid';

import { addCart, removeFromCart } from '../../actions/cartActions.js';

const CartPage = ({}) => {
  const { id } = useParams();
  const location = useLocation();
  const qty = location.search ? Number(location.search.split('=')[1]) : 1
  
  const dispatch = useDispatch();
  const cart = useSelector(state=> state.cart);
  const { cartItems } = cart;

  useEffect(() => {
    if(id) {
      dispatch(addCart(id, qty))
    }
  }, [dispatch, id, qty])

  const removeFromCartHandler = (id) => {
    dispatch(removeFromCart(id))
  }
  const checkoutHandler = () => {
    console.log("checkoutHandler")
  }

  return (
    <Box sx={{ flexGrow: 1 }}>
      <h1> Shopping Cart</h1>
      {cartItems.length === 0 ? (
        <h2>Your cart is empty <Link to='/'>Go Back</Link> </h2>
      ) : (
        <Grid container spacing={2}>
          <Grid item xs={8}>
            <Card sx={{ minWidth: 345 }}>
              <List sx={{ width: '100%', bgcolor: 'background.paper' }}>
                {cartItems.map(item => (
                  <ListItem key={item.product}>
                    <ImageListItem key={item.product} sx={{maxWidth: '120px'}}>
                      <img
                        src={`${item.image}?w=64&h=64&fit=crop&auto=format`}
                        srcSet={`${item.image}?w=64&h=64&fit=crop&auto=format&dpr=2 2x`}
                        alt={item.name}
                        loading="lazy"
                      />
                    </ImageListItem>
                    <Link to={'/products/' + item.product}>
                      <ListItemText key={item.product} primary={item.name} secondary={"$ " + item.price } sx={{margin: '20px', width: '400px'}}/>
                    </Link>
                    <FormControl sx={{ minWidth: '100px' }} size="small">
                      <InputLabel id="demo-select-small">Quantity</InputLabel>
                      <Select
                        labelId="demo-select-small"
                        id="demo-select-small"
                        value={item.qty}
                        label="Quantity"
                        onChange={(e) => dispatch(addCart(item.product, Number(e.target.value)))}
                      >
                        <MenuItem value="">
                          <em>None</em>
                        </MenuItem>
                        {
                          [...Array(item.countInStock).keys()].map((x) => (
                            <MenuItem key={x+1} value={x+1}>{x+1}</MenuItem>
                          ))
                        }
                      </Select>
                    </FormControl>
                    <IconButton aria-label="delete" sx={{ margin: '20px', minWidth: '50px' }}
                      onClick={()=> removeFromCartHandler(item.product)}
                    >
                      <DeleteIcon />
                    </IconButton>
                  </ListItem>
                ))}
              </List>
            </Card>
          </Grid>
          <Grid item xs={4}>
            <Card sx={{ maxWidth: 345 }}>
              <CardContent>
                <Typography gutterBottom variant="h4" component="div">
                  SUMTOTAL ({cartItems.reduce((acc, item) => acc + item.qty, 0)}) ITEMS
                </Typography>
                <Typography variant="h6" color="text.secondary">
                ${cartItems.reduce((acc, item) => acc + item.qty * item.price, 0).toFixed(2)}
                </Typography>
              </CardContent>
              <CardActions>
                <Button variant="contained" sx={{width: '100%'}}
                  disabled={cartItems.length === 0}
                  onClick={checkoutHandler}
                >
                  PROCEED TO CHECKOUT
                </Button>
              </CardActions>
            </Card>
          </Grid>
        </Grid>
      )}
    </Box>
  );
}

export default CartPage;