import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Grid from '@mui/material/Grid';
import CircularProgress from '@mui/material/CircularProgress';

import { listProducts } from '../../actions/productActions.js';
import ProductList from '../ProductPage/Product/ProductList.js';

const HomePage = () => {
  const dispatch = useDispatch();  
  const productList = useSelector(state  => state.productList);
  const { error, loading, products } = productList;
  
  useEffect(() => {
    dispatch(listProducts()) 
  }, [dispatch])

  return (
    <Grid sx={{ flexGrow: 1 }} container spacing={4}>
      {
        loading ? <Grid container justifyContent="center" pt={30}><CircularProgress disableShrink /></Grid>
                : error ? <h3> {error}</h3>
                        : <Grid item xs={12}>
                            <ProductList products={products}/>
                          </Grid>
      }      
    </Grid>
  );
}

export default HomePage;