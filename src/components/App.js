import React, { Component } from "react";
import { Container, Box } from "@mui/material";
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';

import '../styles/App.css';
import Header from './UI/Header.js';
import Footer from './UI/Footer.js';
import ContactSpeed from './Contact/ContactSpeedDial.js';
import HomePage from './HomePage/HomePage.js';
import ProductPage from './ProductPage/ProductPage.js';
import CartPage from './CartPage/CartPage.js';
import NotFound from './NotFoundPage/NotFound.js'

class App extends Component {
  render() {
    return (
      <Router>
        <div className="App">
          <Header></Header>
          <main className="py-3">
            <ContactSpeed></ContactSpeed>
            <Container fixed>
              <Box py={3} pt={9} display="flex" flexWrap="wrap">
                <Routes>
                  <Route path='' component={HomePage} element={<HomePage />} exact />
                  <Route path='products/:id' component={ProductPage} element={<ProductPage />}/>
                  <Route path='cart/:id' component={CartPage} element={<CartPage />}/>
                  <Route path='*' component={NotFound} element={<NotFound/>}/>
                </Routes>
              </Box>
            </Container>
          </main>
          <Footer></Footer>
        </div>
      </Router>
    );
  }
}

export default App;
