import * as React from 'react';
import Box from '@mui/material/Box';
import Rating from '@mui/material/Rating';
import Typography from '@mui/material/Typography';

const BasicRating = (props) => {
  const [value, setValue] = React.useState(props.value);
  return (
    <Box
      sx={{
        width: 250,
        display: 'flex',
        mt: 2
      }}
    >
      <Rating
        name="simple-controlled"
        value={value}
        onChange={(event, newValue) => {
          setValue(newValue);
        }}
      />
      <Box sx={{ ml: 1, mt: 0.5 }}>{props.numReviews} reviews</Box>
    </Box>
  );
}

export default BasicRating;