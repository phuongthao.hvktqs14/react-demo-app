import React from 'react';
import Container from '@mui/material/Container';
import Box from '@mui/material/Box';
import Link from '@mui/material/Link';
import Typography from '@mui/material/Typography';

const Footer = () => {
  const content = {
    'copy': 'Copyright © 2022 My Store.',
  };

  return (
    <footer>
      <Container maxWidth="lg">
        <Box py={3} display="flex" flexWrap="wrap" justifyContent="center">
          <Typography color="textSecondary" component="p" variant="caption" gutterBottom={false}>{content['copy']}</Typography>
        </Box>
      </Container>
    </footer>
);
};
export default Footer;
