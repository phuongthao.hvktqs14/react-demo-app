import { axiosClient } from './axiosClient.js';

export const productApi = {    
    async getAll() {
        const response = await axiosClient.get('api/products');
        const data = await JSON.parse(JSON.stringify(eval('('+response.data+')')));
        return data.data
    },

    async get(id) {
        const url = `api/products/${id}`;
        const response = await axiosClient.get(url);
        const data = await JSON.parse(JSON.stringify(eval('('+response.data+')')));
        return data.data;
    },

    add(data) {
        const url = `/products/`;
        return axiosClient.post(url, data);
    },

    update(data) {
        const url = `/products/${data.id}/`;
        return axiosClient.patch(url, data);
    },

    remove(id) {
        const url = `/products/${id}/`;
        return axiosClient.delete(url);
    },

}
