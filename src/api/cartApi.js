import { axiosClient } from './axiosClient.js';

export const cartApi = {
    async add(id) {
        const url = `api/products/${id}`;
        const response = await axiosClient.get(url);
        const data = await JSON.parse(JSON.stringify(eval('('+response.data+')')));
        return data.data;
    }
}
